use std::io;

fn main() {
    println!("Please input an integer which greater 1: ");

    let mut str = String::new();
    match io::stdin().read_line(&mut str) {
        Ok(_n) => {}
        Err(error) => println!("Error: {}", error),
    }
    let n: u32 = str.trim().parse().unwrap();

    println!("Result:\n{:?}", seive(n));
}

fn seive(n: u32) -> Vec<u32> {
    if n > 1 {
        // A boolean vector to compare.
        let mut bool_vec: Vec<bool> = Vec::new();
        // A vector to contains output result.
        let mut output_vec: Vec<u32> = Vec::new();

        // Insert all true value by default (all numbers are primitive numbers).
        for _i in 0..n+1 {
            bool_vec.push(true);
        }

        // Rate from 2 (because 0 and 1 are already the primitive numbers yet).
        let mut p = 2;
        while p*p <= n {
            if bool_vec[p as usize] == true {
                // All mutiples of p is false
                let mut mp = p * 2;
                while mp <= n {
                    bool_vec[mp as usize] = false;
                    mp += p;
                }
            }
            p += 1;
        }

        // From 2, if  to output_vec.
        let mut i = 2;
        while i <= n {
            if bool_vec[i as usize] == true {
                output_vec.push(i);
            }
            i += 1;
        }

        output_vec

    } else {
        println!("Try again! You've not inputed a number which greater than 1: {}.", n);
        Vec::new() // Return a blank vector to avoid return error.
    }
}
