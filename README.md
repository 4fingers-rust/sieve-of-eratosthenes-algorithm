# Sieve of Eratosthenes Algorithm

# Overall

A prime number is a whole number that has exactly two factors, 1 and itself. The Sieve of Eratosthenes is an ancient algorithm that can help us find all prime numbers up to any given limit.

This is Rust implemention of above algorithm.

# Test the program

First of all, you must install **Rust** first. After that, you have to clone this repository to your local hard drive.

Run:

``` cd sieve-of-eratosthenes-algorithm && cargo run```

Have fun!